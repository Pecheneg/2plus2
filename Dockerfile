FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build

RUN apt-get update
RUN apt-get install curl -y

RUN mkdir /dottrace \
    && curl -SL https://download.jetbrains.com/resharper/dotUltimate.2021.1.3/JetBrains.dotTrace.CommandLineTools.linux-x64.2021.1.3.tar.gz \
    | tar xzf - -C /dottrace
    
RUN mkdir /dotmemory \
    && curl -SL https://download.jetbrains.com/resharper/dotUltimate.2021.1.3/JetBrains.dotMemory.Console.linux-x64.2021.1.3.tar.gz \
    | tar xzf - -C /dotmemory

COPY /2plus2/ 2plus2/


FROM build AS publish
WORKDIR "/2plus2"
RUN dotnet restore "2plus2.csproj"
RUN dotnet publish "2plus2.csproj" -c Release -o /app/publish


FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS runtime

COPY --from=publish /app/publish /app/
COPY --from=build /dottrace /app/dottrace
COPY --from=build /dotmemory /app/dotmemory
#COPY ../../2plus2/appsettings.json  /app/appsettings.json

WORKDIR /app
ENTRYPOINT ["dotnet", "2plus2.dll"]